import React from "react";
import { Link } from 'react-router-dom';


const Home = () => {
  return (
    <div>
      <main role="main mt-5">
        <div className="jumbotron mt-5">
          <div className="container">
            <h1 className="display-3">Hello, world!</h1>
            <h5 className="mb-3">Welcome to Afif Firdaus blog</h5>
            <p><Link className="btn btn-dark btn-lg" to="/about" role="button">View my profile &raquo;</Link></p>
          </div>
        </div>

        <div className="container">
        </div>
      </main>



      <footer class="container blog-footer">
        <hr />
        <p>&copy; Afif Firdaus</p>
      </footer>
    </div>
  )
};

export default Home;