import React from "react";

const Contact = () => {
  return (
    <div className="col-md-4 order-md-1 container mb-5 mt-5">
      <br />
      <br />
      <br />
      <br />
      <form className="needs-validation card-body shadow-sm mt5 bg-light-gray" novalidate>
        <h1 className="mb-4 font-weight-bold text-center text-dark"> Contact me!</h1>

        <div className="row">
          <div className="col-md-6 mb-3">
            <label for="firstName">First name</label>
            <input type="text" className="form-control" id="firstName" placeholder="" value="" required />
            <div className="invalid-feedback">
              Valid first name is required.
            </div>
          </div>
          <div className="col-md-6 mb-3">
            <label for="lastName">Last name</label>
            <input type="text" className="form-control" id="lastName" placeholder="" value="" required />
            <div className="invalid-feedback">
              Valid last name is required.
            </div>
          </div>
        </div>

        <div className="mb-4">
          <label for="email">Email</label>
          <input type="email" className="form-control" id="email" placeholder="you@example.com" />
          <div className="invalid-feedback">
            Please enter a valid email address.
          </div>
        </div>

        <button className="btn btn-dark btn-lg btn-block" type="submit"><svg width="1em" height="1em" viewBox="0 0 16 16" className="bi bi-envelope-fill mb-1" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
          <path fill-rule="evenodd" d="M.05 3.555A2 2 0 0 1 2 2h12a2 2 0 0 1 1.95 1.555L8 8.414.05 3.555zM0 4.697v7.104l5.803-3.558L0 4.697zM6.761 8.83l-6.57 4.027A2 2 0 0 0 2 14h12a2 2 0 0 0 1.808-1.144l-6.57-4.027L8 9.586l-1.239-.757zm3.436-.586L16 11.801V4.697l-5.803 3.546z" />
        </svg>  Submit</button>
      </form>
    </div>
  );
};

export default Contact;